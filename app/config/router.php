<? /**
 * Range-Ray API PROGRAMM 2016
 * @category   Range-Ray CMS
 * @author     gaibovich.alexey <rangeray@mail.ru>
 * @copyright  Copyright (c) 2014-2016, 3dchita.ru
 * @license    http://www.3dchita.ru/info/disclaimer
 * @link       http://www.3dchita.ru/projects/
 * ${FILENAME}
 * biling
 * 2015-07-09
 */

use Phalcon\Mvc\Router;

$router = new Router();

$router->add("/", [
    "controller" => "users",
    "action" => "search"
]);

return $router;
