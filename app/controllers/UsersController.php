<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Mvc\View;
use Phalcon\Http\Response;

class UsersController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for users
     */
    public function searchAction()
    {
        $data = array();
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'Users', $_POST);
            $this->persistent->parameters = $query->getParams();

        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }

        $users = Users::find($parameters);
        if (count($users) == 0) {
            $data['error'] = 'not found';
            $this->dispatcher->forward(array(
                "controller" => "users",
                "action" => "index"
            ));
        }
        $paginator = new Paginator(array(
            'data' => $users,
            'limit'=> 10,
            'page' => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a user
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $user = Users::findFirstByid($id);
            if (!$user) {
                $this->flash->error("пользователь не найден");

                $this->dispatcher->forward(array(
                    'controller' => "users",
                    'action' => 'index'
                ));

                return;
            }

            $this->view->id = $user->id;

            $this->tag->setDefault("id", $user->id);
            $this->tag->setDefault("name", $user->name);
            $this->tag->setDefault("balance", $user->balance);
            
        }
    }

    /**
     * Creates a new user
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward(array(
                'controller' => "users",
                'action' => 'index'
            ));

            return;
        }

        $user = new Users();
        $user->name = $this->request->getPost("name");
        $user->balance = $this->request->getPost("balance");
        

        if (!$user->save()) {
            foreach ($user->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward(array(
                'controller' => "users",
                'action' => 'new'
            ));

            return;
        }

        $this->flash->success("user was created successfully");

        $this->dispatcher->forward(array(
            'controller' => "users",
            'action' => 'index'
        ));
    }

    /**
     * Saves a user edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward(array(
                'controller' => "users",
                'action' => 'index'
            ));

            return;
        }

        $id = $this->request->getPost("id");
        $user = Users::findFirstByid($id);

        if (!$user) {
            $this->flash->error("user does not exist " . $id);

            $this->dispatcher->forward(array(
                'controller' => "users",
                'action' => 'index'
            ));

            return;
        }

        $user->name = $this->request->getPost("name");
        $user->balance = $this->request->getPost("balance");
        

        if (!$user->save()) {

            foreach ($user->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward(array(
                'controller' => "users",
                'action' => 'edit',
                'params' => array($user->id)
            ));

            return;
        }

        $this->flash->success("user was updated successfully");

        $this->dispatcher->forward(array(
            'controller' => "users",
            'action' => 'index'
        ));
    }

    /**
     * Deletes a user
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $user = Users::findFirstByid($id);
        if (!$user) {
            $this->flash->error("user was not found");

            $this->dispatcher->forward(array(
                'controller' => "users",
                'action' => 'index'
            ));

            return;
        }

        if (!$user->delete()) {

            foreach ($user->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward(array(
                'controller' => "users",
                'action' => 'search'
            ));

            return;
        }

        $this->flash->success("user was deleted successfully");

        $this->dispatcher->forward(array(
            'controller' => "users",
            'action' => "index"
        ));
    }

    /**
     *  автозаполнитель пользователей
    */

    public function aggregateAction(){

        $generator = new \Nubs\RandomNameGenerator\All(
            [
                new \Nubs\RandomNameGenerator\Alliteration(),
                //new \Nubs\RandomNameGenerator\create()
            ]
        );


        $this->flash->success("10 человек успешно созданы");


        foreach (range(1, 10, 1) as $actions) {
            //echo 'its agregate '. $generator->getName();
            $user = new Users();
            $user->name = $generator->getName();
            $user->balance = rand(1.00, 60000.00);
            $user->save();
        }

        $this->dispatcher->forward(array(
            'controller' => "users",
            'action' => 'search'
        ));

    }

    /**
    перечесление денежных средств
     */

    public function transferAction(){
        $this->view->disable(); // отключаем вью

        // получаем параметры
        $transmitUserID = $this->request->getPost('transmitUserID');
        $receivingUserID = $this->request->getPost('receivingUserID');
        $transferAmount = $this->request->getPost('transferAmount');

        if($transmitUserID && $receivingUserID && $transmitUserID !== $receivingUserID && $transferAmount > 0) { // проверка входных данных



            $transmitUser = Users::findFirstByid($transmitUserID); // поиск отправителя
            $receivingUser = Users::findFirstByid($receivingUserID); // поиск получателя

            if ($transmitUser || $receivingUser ) { // проверка есть ли отправитель и получатель

                if($transferAmount <= $transmitUser->balance){ // проверка есть ли на балансе доступные средства

                    // transaction
                    $transmitUserBalance = [
                        'balance' => $transmitUser->balance - $transferAmount // списываем
                    ];

                    $receivingUserBalance = [
                        'balance' => $receivingUser->balance + $transferAmount // зачисляем
                    ];


                    if($transmitUser->update($transmitUserBalance, ['balance']) &&
                        $receivingUser->update($receivingUserBalance, ['balance'])) { // проверка на ошибки при сохранении

                        echo json_encode([
                            'transferAmount' => $transferAmount,
                            'transmit' => [
                                'id' => $transmitUser->id,
                                'name' => $transmitUser->name,
                                'balance' => $transmitUser->balance,
                            ],
                            'receiving' => [
                                'id' => $receivingUser->id,
                                'name' => $receivingUser->name,
                                'balance' => $receivingUser->balance,
                            ],
                            'success' => 'транзакция выполнена успешно',
                            'error' => false
                        ]);

                    } else {

                        echo json_encode([
                            'error' => 'транзакция не выполнена'
                        ]);
                    }


                } else {
                    echo json_encode([
                        'error' => 'сумма перевода превышает лимит средств на балансе отправителя'
                    ]);
                }

            } else {
                echo json_encode([
                    'error' => 'пользователи небыли найдены в системе'
                ]);
            }




        } else {
            echo json_encode([
                'error' => 'пустые либо неверные параметры запроса',
            ]);
        }



    }

    public function searchjsonAction(){
        $this->view->disable(); // отключаем вью
        $numberPage = $this->request->getPost('page') ? $this->request->get('page') : 1;
        $id = $this->request->getPost('id');
        $name = $this->request->getPost('name');
        $exclusion = $this->request->getPost('exclusion');
        $data['$_POST'] = $_POST;


        $parameters = Users::conditions($id, $name, $exclusion);

        $users = Users::find($parameters);

        $paginator = new Paginator(array(
            'data' => $users,
            'limit'=> 10,
            'page' => $numberPage
        ));

        foreach ($paginator->getPaginate()->items as $user) {
            $data['users'][] = array(
                'id'   => $user->id,
                'name' => $user->name,
                'balance' => $user->balance
            );
        }
        $paginator->getPaginate()->items = false;
        $data['paginate'] = [
            "first" => $paginator->getPaginate()->first,
            "before" => $paginator->getPaginate()->before,
            "last" => $paginator->getPaginate()->last,
            "next" => $paginator->getPaginate()->next,
            "current" => $paginator->getPaginate()->current,
            "total_items" => $paginator->getPaginate()->total_items,
            "limit" => $paginator->getPaginate()->limit,
            "total_pages" => $paginator->getPaginate()->total_pages
        ];
        echo json_encode($data);



    }



}
