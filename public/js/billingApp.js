/**
 * Created by rangeray on 01.08.16.
 */
(function() {
    var app = angular.module('billingApp', ['ngSanitize']);

    app.controller('ActionsController', function($scope, $http){



        this.step = 1; // шаг первый
        this.receivingUserName = 'выберите получателя, воспользуйтесь формой поиска';
        this.row = 0;




        this.FindUsers = function(params){

            // запрашиваем параметры поиска
            //var params = this.getparmSearch();


            $http.post("/users/searchjson", params)
                .success(function(data) {



                    $scope.action.divide || ($scope.data2 = data);

                    if($scope.action.divide){
                        $scope.data1 = data;
                    }



                    //$scope.users = data.users;
                    //$scope.paginate = data.paginate;


                })
                .error(function(data, status, headers, config) {

                });

        };


        // устанавливаем текущего пользователя отправителя денежных средств
        this.setUser = function(userID, userName, userBalance){
            this.setStep(1); // <-- инициализируем первый шаг

            this.row = 0;

            this.divide = false; // запрашивать второй список
            this.FindUsers({'exclusion' : userID}); // <--- исключаем пользователя из поиска

            this.userID = userID;
            this.userName = userName;
            this.userBalance = userBalance;
            this.summ = userBalance;

        };




        this.setRow = function(newValue){
            this.row = newValue;
        };

        this.isRow = function(rowID){
            return this.row === rowID;
        };

        this.setStep = function(step){
            this.step = step
        };

        this.verify = function(value){
            this.summ = (this.userBalance - value) > 0 ? this.userBalance - value : 0;
        };

        this.Transfer = function(){
            var data = {
                'transmitUserID' : this.userID,
                'receivingUserID' : this.receivingUserID,
                'transferAmount' : this.setStepSumm
            };
            $http.post("/users/transfer", data)
                .success(function(data) {


                    $scope.error = data.error;
                    $scope.success = data.success;
                    $scope.transmit = data.transmit;
                    $scope.receiving = data.receiving;
                    $scope.transferAmount = data.transferAmount;

                    //$scope.action.userBalance = data.transmit.balance;

                    //$scope.action.receivingUserBalance = data.receiving.balance;
                    //$scope.action.receivingUserName = data.receiving.name;

                    $scope.action.divide = true;
                    $scope.action.FindUsers({'page' : $scope.data1.paginate.current});

                    $scope.action.setStepSumm = '';
                    //console.log(data)

                })
                .error(function(data, status, headers, config) {});

        };

        this.divide = true;
        this.FindUsers();


    });




    // для использования POST метода
    // преабразуем запрос AngularJS + PHP. Заставляем $http-сервис веcти себя как jQuery.ajax()
    app.config( function( $httpProvider ) {    // [url]http://habrahabr.ru/post/181009/[/url]
        $httpProvider.defaults.headers.post[ 'Content-Type' ] = 'application/x-www-form-urlencoded;charset=utf-8';
        $httpProvider.defaults.transformRequest = function( data ) {
            return angular.isObject( data ) && String( data ) !== '[object File]' ? angular.toParam( data ) : data;
        };
    });



})();